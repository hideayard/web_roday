<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
error_reporting(0);
session_start();
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);

include("config/functions.php");  
require_once("tokenlogin.php");

$secret = "B15m1ll4#";

$file = basename($_SERVER['PHP_SELF']);
$filename = (explode(".",$file))[0];

{
    $token = isset($_POST['token']) ? $_POST['token'] : ""; 
    $jsondata = isset($_POST['jsondata']) ? $_POST['jsondata'] : ""; 
    $data = json_decode($jsondata);
    // var_dump($data);die;
    $status = false;
    $msg = "Please Input Token!";
    if (json_last_error() === JSON_ERROR_NONE) {
        $status = true;
        $msg = "JSON OK";
        //do something with $json. It's ready to use
    } else {
        $status = false;
        $msg = "JSON ERROR";
    }

    $otl = new TokenLogin($secret);
    if($token!="")
    {
        try {
            $payload = $otl->validate_token($token);
    
        if ($payload) {
                $status = true;
                $msg =  "Valid token!";// You are user #{$payload->uid}";
                // $hasil = $payload;
                //action save to DB when token valid
            } else {
                $status = false;
                $msg =  "Invalid token";
            }
        } catch (Exception $e) {
                $status = false;
                $msg = 'Caught exception: '.  $e->getMessage();
        }
    }

    if($status == true)
    {
        $id_user = $payload->uid;
        $tipe = $payload->utipe;
      
        $mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
        $type = isset($_POST['type']) ? $_POST['type'] : ""; 
        
        if($mode == "get")
        {
            $sql = "SELECT IFNULL(no_resi, '') as no_resi,IFNULL(asal, '') as asal,IFNULL(tujuan, '') as tujuan from transaksi where created_by = '".$payload->uid."' and status_transaksi ='ON PROCESS' order by id_transaksi desc";
            $hasil = $db->rawQuery($sql);
            if($hasil)
            {   $status = true;
                $msg = "Get Data Success!";
            }
            else
            {
                $status = false;
                $msg = "Get Data Error".$db->getLastError();
            }
            echo json_encode( array("status" => $status,"info" => $sql, "messages" => $msg,"hasil" => $hasil ,"resi" => "") );


        }
        else if($mode == "proses")
        {
            $sql = "SELECT IFNULL(no_resi, '') as no_resi,IFNULL(asal, '') as asal,IFNULL(tujuan, '') as tujuan from transaksi where created_by = '".$payload->uid."' and status_transaksi ='ON PROCESS' order by id_transaksi desc";
            $hasil = $db->rawQuery($sql);
            if($hasil)
            {   $status = true;
                $msg = "Get Data Success!";
            }
            else
            {
                $status = false;
                $msg = "Get Data Error".$db->getLastError();
            }
            echo json_encode( array("status" => $status,"info" => $sql, "messages" => $msg,"hasil" => $hasil ,"resi" => "") );


        }
        else if($mode == "berangkat")
        {
            try{

                $db->startTransaction();
                $resi = isset($_POST['resi']) ? $_POST['resi'] : "";
                $po_name = isset($_POST['po']) ? $_POST['po'] : "";
                $po_telp = isset($_POST['hp_po']) ? $_POST['hp_po'] : "";

                $sql = "SELECT * from transaksi where no_resi = '".$resi."'";
                $hasil = $db->rawQuery($sql);
                if($hasil)
                {   
                    $data = Array (
                        // 'po_name' => $db->inc($item_qty),
                        // 'active' => $db->not()
                        "status_transaksi" => "BERANGKAT KE AGEN TUJUAN" ,
                        "po_name" => $po_name ,
                        "po_telp" => $po_telp,
                    );
                    $db->where ('no_resi', $resi);
                    if ($db->update ('transaksi', $data))
                    {
                        $status = true;
                        $msg = "Input Data Success!";
                    }
                }
                else
                {
                    $status = false;
                    $msg = "Input Data Error".$db->getLastError();
                }

            }
            catch (Exception $e) 
            {
                $db->rollback();
                $status = false;
                $msg = 'Caught exception: '.  $e->getMessage();
            }
            echo json_encode( array("status" => $status,"info" => 'sukses', "messages" => $msg,"hasil" => $hasil ,"resi" => $resi ." , ".$po_name." , ".$po_hp) );

        }
        else if($mode == "get_po")
        {
            $sql = "SELECT IFNULL(nama_po_bus, '') as nama,IFNULL(no_hp_po_bus, '') as hp from po_bus order by id_po_bus desc";
            $hasil = $db->rawQuery($sql);
            if($hasil)
            {   $status = true;
                $msg = "Get Data Success!";
            }
            else
            {
                $status = false;
                $msg = "Get Data Error".$db->getLastError();
            }
            echo json_encode( array("status" => $status,"info" => $sql, "messages" => $msg,"hasil" => $hasil ,"resi" => "") );


        }

        else if($mode == "")
        {

          switch($mode)
          {
            case "get" : {$transaction_status = 2;}break;
            case "submit" : {$transaction_status = 1;}break;
            case "delete" : {$transaction_status = 0;}break;
            default : {$transaction_status = 1;}break;
          }
      
            $tgl = (new \DateTime())->format('Y-m-d H:i:s');
            $ts_no = $payload->uid .".". round(microtime(true) * 1000);
            $qdata = $sdata = $logdata =  Array(); 
            $total = 0; 
            $info = "";

            $qdata[] = Array (  "id_transaksi" => null,
                                "no_resi" => $ts_no,
                                "berat_paket" => $data->{'berat'},
                                "volume_paket" => $data->{'volume'},
                                "asal" => $data->{'asal'},
                                "tujuan" => $data->{'tujuan'},
                                "tarif" => $data->{'tarif'},
                                "isi_paket" => $data->{'isi_paket'},
                                "nama_pengirim" => $data->{'nama_pengirim'},
                                "alamat_pengirim" => $data->{'alamat_pengirim'},
                                "hp_pengirim" => $data->{'hp_pengirim'},
                                "wa_pengirim" => $data->{'wa_pengirim'},
                                "nama_penerima" => $data->{'nama_penerima'},
                                "alamat_penerima" => $data->{'alamat_penerima'},
                                "hp_penerima" => $data->{'hp_penerima'},
                                "wa_penerima" => $data->{'wa_penerima'},
                                "status_transaksi" => "ON PROCESS" ,
                                "created_by" => $payload->uid,
                                "created_at" => $tgl,
                                "modified_by" => $payload->uid,
                                "modified_at" => $tgl,
                                "is_deleted" => "0"
                            );
                $db->startTransaction();
                $ids = $db->insertMulti('transaksi', $qdata);
                if(!$ids) {
                    echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $msg,"resi" => "" ) );

                } else {

                    {   $db->commit(); 
                        $info .=  ' , Log Saved'; $msg = "Transaction Success.!";
                        echo json_encode( array("status" => true,"info" => $info,"messages" => $msg,"resi" => $ts_no ) );
                    }

                    
                    
                }
            }
            else
            {
               

            }
    }
    else
    {
        echo json_encode( array("status" => false,"info" => "Please check token or try to login again!","messages" => $msg,"resi" => "" ) );
    }
 

}

$db->disconnect();
?>