<?php

require_once('vendor/autoload.php');
use \Firebase\JWT\JWT;

class TokenLogin {
   private $secret;

   function __construct($secret) {
      $this->secret = $secret;
   }

   function create_token($uid, $token_duration = 24 /* Hours */) {
      $uid = intval($uid);
      if (!($uid > 0)) return;

      $now = time();

      $data = array();
      $data['uid'] = $uid;
      $data['iat'] = $now;
      $data['exp'] = $now + $token_duration * (60 * 60);

      return JWT::encode($data, $this->secret);
   }

   function create_login_token($uid,$uname,$unama,$uemail,$utipe,$ufoto,$user_nama_agen, $token_duration = 24 /* Hours */) {
      $uid = intval($uid);
      if (!($uid > 0)) return;

      $now = time();

      $data = array();
      $data['uid'] = $uid;
      $data['uname'] = $uname;
      $data['unama'] = $unama;
      $data['uemail'] = $uemail;
      $data['utipe'] = $utipe;
      $data['ufoto'] = $ufoto;
      $data['user_nama_agen'] = $user_nama_agen;
      $data['iat'] = $now;
      $data['exp'] = $now + $token_duration * (60 * 60);

      return JWT::encode($data, $this->secret);
   }

   function validate_token($token) {
      try {
         $payload = JWT::decode($token, $this->secret, array('HS256'));
         if (!$payload) return FALSE;
         return json_decode(json_encode($payload));
      } catch (Exception $e) {
         // die(var_dump($e));
         return FALSE;
      }
   }
}

?>