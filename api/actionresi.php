<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
error_reporting(0);
session_start();
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);

include("config/functions.php");  
require_once("tokenlogin.php");

$secret = "B15m1ll4#";

$file = basename($_SERVER['PHP_SELF']);
$filename = (explode(".",$file))[0];

{
    $token = isset($_POST['token']) ? $_POST['token'] : ""; 
    $jsondata = isset($_POST['jsondata']) ? $_POST['jsondata'] : ""; 
    $data = json_decode($jsondata);
    // var_dump($data);die;
    $status = false;
    $msg = "Please Input Token!";
    if (json_last_error() === JSON_ERROR_NONE) {
        $status = true;
        $msg = "JSON OK";
        //do something with $json. It's ready to use
    } else {
        $status = false;
        $msg = "JSON ERROR";
    }

    $otl = new TokenLogin($secret);
    if($token!="")
    {
        try {
            $payload = $otl->validate_token($token);
    
        if ($payload) {
                $status = true;
                $msg =  "Valid token!";// You are user #{$payload->uid}";
                // $hasil = $payload;
                //action save to DB when token valid
            } else {
                $status = false;
                $msg =  "Invalid token";
            }
        } catch (Exception $e) {
                $status = false;
                $msg = 'Caught exception: '.  $e->getMessage();
        }
    }

    if($status == true)
    {
        $id_user = $payload->uid;
        $tipe = $payload->utipe;

        $mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
        $resi = isset($_POST['resi']) ? $_POST['resi'] : ""; 
    
        if($mode == "lacak")
        {
            try{

                $db->where ("no_resi", $resi);
                $transaksi = $db->getOne ("transaksi");

                if($transaksi["status_transaksi"])
                {   $status = true;
                    $msg = "Get Data Success!";
                    $hasil = $transaksi["status_transaksi"];
                }
                else
                {
                    $status = false;
                    $msg = "Get Data Error!";
                }
            }
            catch (Exception $e) 
            {
                $db->rollback();
                $status = false;
                $msg = 'Caught exception: '.  $e->getMessage();
            }
            echo json_encode( array("status" => $status,"info" => 'sukses', "messages" => $msg,"hasil" => $hasil ,"resi" => $resi) );

        }
        else if($mode == "terima")
        {
            $hasil = true;
            try{

                $db->where ("no_resi", $resi);
                $transaksi = $db->getOne ("transaksi");

                if($transaksi["status_transaksi"] == "BERANGKAT KE AGEN TUJUAN")
                {
                    $data = Array (
                        "status_transaksi" => "SAMPAI DI AGEN TUJUAN" 
                    );
                    $db->where ('no_resi', $resi);
                    if ($db->update ('transaksi', $data))
                    {
                        $status = true;
                        $info = "Sukses";
                        $msg = "Paket sudah di terima di Agen tujuan!";
                    }
                    else
                    {
                        $info = "Gagal";
                        $status = false;
                        $msg = "Error!";
                    }
                }
                else
                {
                    $info = "Gagal";
                    $status = false;
                    $msg = "Paket belum berangkat.!";
                }
               
            }
            catch (Exception $e) 
            {
                $db->rollback();
                $status = false;
                $msg = 'Caught exception: '.  $e->getMessage();
            }
             echo json_encode( array("status" => $status,"info" => $info, "messages" => $msg,"hasil" => $hasil ,"resi" => $resi) );

        }
        
    }
    else
    {
        $mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
        $resi = isset($_POST['resi']) ? $_POST['resi'] : ""; 
        
        if($mode == "lacak")
        {
            try{

                $db->where ("no_resi", $resi);
                $transaksi = $db->getOne ("transaksi");

                if($transaksi["status_transaksi"])
                {   $status = true;
                    $msg = "Get Data Success!";
                    $hasil = $transaksi["status_transaksi"];
                }
                else
                {
                    $status = false;
                    $msg = "Get Data Error!";
                }
            }
            catch (Exception $e) 
            {
                $db->rollback();
                $status = false;
                $msg = 'Caught exception: '.  $e->getMessage();
            }
             echo json_encode( array("status" => $status,"info" => 'sukses', "messages" => $msg,"hasil" => $hasil ,"resi" => $resi) );

        }
    }
 

}

$db->disconnect();
?>