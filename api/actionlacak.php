<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// error_reporting(0);
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);

include("config/functions.php");  

$mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
$resi = isset($_POST['resi']) ? $_POST['resi'] : ""; 
// var_dump($resi);
if($mode == "lacak")
{
    try{

        $db->where ("no_resi", $resi);
        $transaksi = $db->getOne ("transaksi");

        if($transaksi["status_transaksi"])
        {   $status = true;
            $msg = "Get Data Success!";
            $hasil = $transaksi["status_transaksi"];
        }
        else
        {
            $status = false;
            $msg = "Get Data Error!";
        }
        echo json_encode( array("status" => $status,"info" => 'sukses', "messages" => $msg,"hasil" => $hasil ,"resi" => $resi) );
    }
    catch (Exception $e) 
    {
        $db->rollback();
        $status = false;
        $msg = 'Caught exception: '.  $e->getMessage();
        echo json_encode( array("status" => false,"info" => 'gagal', "messages" => $msg,"hasil" => $hasil ,"resi" => $resi) );
    }

}


// $db->disconnect();
?>